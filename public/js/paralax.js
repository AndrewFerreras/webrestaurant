$(document).ready(function () {
   
    $(window).scroll(function () { 
        var windowWidtch= $(window).width();

        if(windowWidtch>800){
            
            var Scroll = $(window).scrollTop();
            $('header .textos').css({
                'transform': 'translate(0px,' + Scroll / 2 + '%)'
            });

            $('.acerca-de article').css({
                'transform': 'translate(0px, -' + Scroll / 4 + '%)'
            });
        }
    });


    $(window).resize(function(){
		var windowWidth = $(window).width();
		if (windowWidth < 800){
			$('.acerca-de article').css({
					'transform': 'translate(0px, 0px)'
			});
		}
	});
});