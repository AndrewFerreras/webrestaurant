$(document).ready(function(){

   
    //efecto menu
    $('.menu a').each(function(index, elemento){
		$(this).css({
			'top': '-200px'
		});

		$(this).animate({
			top: '0'
		},2000 + (index * 500));
	});
     //efecto header
     if($(window).width()>800){
        $('header .textos').css({
            opacity: 0,
            marginTop:0
        });

        $('header .textos').animate({
            opacity: 1,
            marginTop:'-52px'
        },1500);
     }

     //scroll elementos menu
    var asercaDe=$('#acerca-de').offset().top;
    var menu=$('#menu').offset().top;
    var galeria=$('#galeria').offset().top;
    var ubicacion=$('#ubicacion').offset().top;

    $('#btn-acerca').on('click',function(e)
    {   e.preventDefault();
        $('html, body').animate({
            scrollTop: asercaDe -100
        },500);
    });

    $('#btn-Menu').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: menu + 590
        },500);
    });

    $('#btn-Galeria').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: galeria 
        },500);
    });

    $('#btn-Ubicacion').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: ubicacion +173 
        },500);
    });
   
});