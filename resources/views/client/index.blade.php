<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    <title> WebRestaurant</title>
</head>
<body>
    <header>
        <div class="contenedor">
            <nav class= "menu">
                <a href="#" id="btn-acerca">Acerca de</a>
                <a href="#" id="btn-Menu">Menu</a>
                <a href="#" id="btn-Galeria">Galeria</a>
                <a href="#" id="btn-Ubicacion">Ubicacion</a>
                <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
                   
            </nav>
            <div class="textos">
                <h1 class="nombre">Web <span>Restaurant</span> </h1>
                <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h3>
            </div>
        </div>
    </header>

            <main>
                <Section class="acerca-de" id="acerca-de">
                    <div class="contenedor">
                        <div class="foto">
                            <img src="{{URL::asset('img/acerca-de.jpg')}}" alt="">
                        </div>
                        <article>
                        <h3>Acerca de</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </article>
                    </div>
                </Section>

                <Section class="menu" id="menu">
                    <div class="contenedor">
                        <h3 class="titulo" id="platillos">Menu</h3>
                        <div class="btn"> <button id="btn-categoria">Ofertas</button></div>
                       
                        <div class="contenedor-menu">
                            <div class="contenedor-menu2">
                            
                            <article>
                                <p class="categoria">De Comer</p>
                                <div class="platillo">
                                    <p class="nombre">Sed sagittis nisl dictum</p>
                                    <p class="precio">$15</p>
                                    <p class="descripcion">Praesent gravida, augue sit amet dignissim rutrum, 
                                    enim tellus suscipit risus.</p>
                                </div>
                                <div class="platillo">
                                    <p class="nombre">Lorem ipsum dolor sit amet</p>
                                    <p class="precio">$10</p>
                                    <p class="descripcion">Vestibulum ac sem id massa tempor vestibulum sed 
                                    ut magna</p>
                                </div>
                                <div class="platillo">
                                    <p class="nombre">Sed sagittis nisl dictum</p>
                                    <p class="precio">$20</p>
                                    <p class="descripcion">Praesent gravida, augue sit amet dignissim rutrum, 
                                    enim tellus suscipit risus.</p>
                                </div>
                                <div class="platillo">
                                    <p class="nombre">Sed sagittis nisl dictum</p>
                                    <p class="precio">$35</p>
                                    <p class="descripcion">Praesent gravida, augue sit amet dignissim rutrum, 
                                    enim tellus suscipit risus.</p>
                                </div>
                            </article>


                            <article>
                                <p class="categoria">De Tomar</p>
                                <div class="platillo">
                                    <p class="nombre">Sed sagittis nisl dictum</p>
                                    <p class="precio">$15</p>
                                    <p class="descripcion">Praesent gravida, augue sit amet dignissim rutrum, 
                                    enim tellus suscipit risus.</p>
                                </div>
                                <div class="platillo">
                                    <p class="nombre">Sed sagittis nisl dictum</p>
                                    <p class="precio">$15</p>
                                    <p class="descripcion">Praesent gravida, augue sit amet dignissim rutrum, 
                                    enim tellus suscipit risus.</p>
                                </div>
                                <div class="platillo">
                                    <p class="nombre">Sed sagittis nisl dictum</p>
                                    <p class="precio">$15</p>
                                    <p class="descripcion">Praesent gravida, augue sit amet dignissim rutrum, 
                                    enim tellus suscipit risus.</p>
                                </div>
                                <div class="platillo">
                                    <p class="nombre">Sed sagittis nisl dictum</p>
                                    <p class="precio">$15</p>
                                    <p class="descripcion">Praesent gravida, augue sit amet dignissim rutrum, 
                                    enim tellus suscipit risus.</p>
                                </div>
                            </article>
                        </div>
                    </div>
                </Section>

                <Section class="galeria" id="galeria">
                    <div class="foto">
                    <img src="{{URL::asset('img/1.jpg')}}" alt="">
                    </div>
                    <div class="foto">
                    <img src="{{URL::asset('img/2.jpg')}}" alt="">
                    </div>

                    <div class="foto">
                    <img src="{{URL::asset('img/3.jpg')}}" alt="">
                    </div>
                    <div class="foto">
                    <img src="{{URL::asset('img/4.jpg')}}" alt="">
                    </div>
                    <div class="foto">
                    <img src="{{URL::asset('img/5.jpg')}}" alt="">
                    </div>
                    <div class="foto">
                    <img src="{{URL::asset('img/6.jpg')}}" alt="">
                    </div>
                    <div class="foto">
                    <img src="{{URL::asset('img/7.jpg')}}" alt="">
                    </div>
                    <div class="foto">
                    <img src="{{URL::asset('img/8.jpg')}}" alt="">
                    </div>
                
                </Section>
                
                <Section class="ubicacion" id="ubicacion">
                    <div class="contenedor">
                        <h3 class=" titulo" >Ubicacion</h3>
                        <div class="direccion">
                            <p class="calle"> Lorem ipsum, dolor sit <br> amet consectetur adipisicing elit. Et, <br> tempora praesentium.</p>
                            <p class="telefono"></p>
                            <p class="correo-electronico">
                            </p>
                        </div>
                        <div class="horario">
                            <h4>Horario</h4>
                            <p class="entre-semanas">Lunes a Viernes <br> 8:00am  9:00pm</p>
                            <p class="fin-semanas">Sabados y Domingos <br> 11:00am  9:00pm</p>
                        </div>
                    </div>
                    
                </Section>
                <section class="mapa">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2302.782250617467!2d-69.8899035525824!3d18.470027183515665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sdo!4v1603401854717!5m2!1ses-419!2sdo" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </section>
            </main>
        <footer>
            <div class="contenedor">
                <div class="copyright">
                    <p>CopyRight Andrew Ferreras @ 2020</p>
                </div>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="{{asset('js/efectos.js')}}"></script>
    <script src="{{asset('js/paralax.js')}}"></script>
</body>
</html>